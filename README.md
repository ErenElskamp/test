![Zmard](Aspose.Words.10e5c0f6-7536-4a49-9018-7d439a5507b5.001.png "Zmard")

## **Format business plan**

_ **Version October 2022** _

[save this file under your name]

[delete this page]

Gerhard Top

Nov 2007, update Okt 2022

gebaseerd op:

- "How to Write a Great Business Plan" by William A. Sahlman, Harvard Business Review, 1997

aangevuld met enkele elementen uit:

- "How to Write a Winning Business Plan" Sanley R.Rich and David E. Gumpert, Harvard Business Review, 2001

- "How Venture Capitalists Evaluate Potential Venture Opportunities" Roberts and Barley, Harvard Business School, December 2004

- LEBD "Conversations in the Structure for Fullfillment"

----

<div align="right">

[Logo]

**NAMECOMP Business Plan**

[NAME, function]<br>
[Phone Number]<br>
[Address]

**Version [Date]**


[FIND AND REPLACE NAMECOMP for your company name]
</div>

<br>
<br>
<br>
<br>
<br>

**COPY NUMBER:** [________]
&copy; 2022 NAMECOMP
All rights reserved.
No part of this document may be reproduced, stored in a retrieval system, or transmitted in any form or by any means (electronic, mechanical, photocopying, recording or otherwise), without the prior written permission of NAMECOMP.

**Trademarks**
All names, products, and services are trademarks or registered trademarks of their respective owners.

----

# Inhoud

1. [Samenvatting](#1-samenvatting)
2. [Team](#2-team)
3. [Opportunity](#3-opportunity)
    1. [Markt](#31-markt)
    2. [Product of service](#32-product-of-service)
    3. [Kasstroom](#33-kasstroom)
    4. [Groeimogelijkheden](#34-groeimogelijkheden)
    5. [Concurrentie](#35-concurrentie)
4. [Context](#4-context)
5. [Risico en opbrengsten](#5-risico-en-opbrengsten)
6. [Bijlagen en data](#6-bijlagen-en-data)
    1. [Bijlagen](#61-bijlagen)
    2. [Data](#62-data)
    3. [Producten, diensten en applicaties](#63-producten-diensten-en-applicaties)
    4. [Interviews en referenties](#64-interviews-en-referenties)
7. [Overige tips uit Harvard Business Reviews](#7-overige-tips-uit-harvard-business-reviews)

----

<br>

# 1. Samenvatting

**Maximaal 2 pagina's op basis waarvan de investeerder besluit het plan echt te gaan lezen.**

- Waar is het doel van dit businessplan?
- Wat drijft je als persoon/team, waar ben je op uit?
- De huidige status van het bedrijf.
- De producten en diensten; voordelen voor de klant.
- De financiële vooruitzichten, doelstellingen voor drie tot zeven jaar.
- De benodigde investering en de return voor de investeerders.

<br>

# 2. Team

Geef dit onderdeel de aandacht die het verdient. Het gaat om de mensen. Als het product niet blijkt te kloppen, maken de juiste mensen de switch op tijd. Voeg eventueel CV toe.

Het ideale team is ervaren, energiek, ondernemend (van het management team tot aan de operatie). De teamleden hebben vaardigheden en ervaring die direct relevant is voor de opportunity. Idealiter hebben zij succesvol samengewerkt in het verleden.

Wie zijn deze mensen? Wat weten ze, wie kennen ze en hoe staan ze bekend?
- Hebben ze het een en ander meegemaakt?
- Wat weten ze van de business? (prod/service/markt/concurrentie/klanten)

Indien het "start up" betreft, wat is de ervaring van de teamleden? Zijn zij bekend bij klanten, leveranciers en/of bij werknemers en hoe?

Vragen die beantwoord worden:
- Waar komt het team vandaan?
- Waar zijn ze opgeleid?
- Wat hebben ze bereikt, professioneel en privé?
- Welke "mislukkingen" zijn er geweest en wat is daar van geleerd?
- Wat is hun reputatie binnen de business community?
- Welke ervaring hebben ze, die direct relevant is voor de opportunity?
- Welke vaardigheden en kennis bezitten ze?
- Hoe realistisch zijn ze over de succeskans van de venture en over de tegenslagen die ze zullen moeten doorstaan?
- Wie zijn er nog meer nodig in het team?
- Zijn ze voorbereid om hoog gekwalificeerd personeel te werven?
- Hoe zullen ze reageren op tegenslagen?
- Hebben ze het lef om onvermijdelijke harde keuzes te maken, die gemaakt moeten worden?
- Hoe gecommitteerd zijn ze aan dit bedrijf?
- Wat is hun motivatie / drijfveer?
- In welke mate zijn ze betrouwbaar in hun afspraken en communiceren ze als iets niet lukt?
- Hoe wordt in hen geïnvesteerd, welk verschil zal dat maken?

<br>

# 3. Opportunity

## 3.1 Markt

Geef eerst antwoord op twee vragen:
- is de totale markt groot en/of snelgroeiend ?
- is de industrie potentieel structureel aantrekkelijk, nu of in potentie ?

Beschrijf rigoureus waarom dat zo is en zo niet, waarom er toch genoeg winst gemaakt kan worden, dat het aantrekkelijk is om te investeren.

## 3.2 Product of service

Dan hoe ga je product bouwen of lanceren?

Wat zijn de mogelijkheden om pilots te doen, mogelijk betaald, om niet eerst helemaal een product of dienst gereed te maken en dan pas te verkopen? Om snel te testen bij klanten? Kan het verkocht worden op basis van een demo of een presentatie?

Geef antwoord op:
- Wie is de klant van de nieuwe venture?
- Hoe neemt de klant beslissingen of hij het product of de service wel of niet koopt?
- In welke mate is het product of de service een aantrekkelijk aankoop voor de klant? 
  - Is dit getest en zijn er verklaringen van klanten?
- Hoe wordt het geprijsd?
- Hoe zal de onderneming alle geïdentificeerde klantsegmenten bereiken?
- Hoeveel kost het in tijd en resources om een klant te werven?
- Hoeveel kost het om het product of de service te leveren?
- Hoeveel kost het om een klant support te leveren?
- Hoe eenvoudig is het om klanten te behouden, wat is hierin kritisch?
- Hoe zal het product zich ontwikkelen?
  - Welke tijd en resources zijn hiervoor beschikbaar? 
  - Hoe worden resources voor productontwikkeling geborgd als er veel vraag is naar klantimplementaties?

## 3.3 Kasstroom

Nadat helder is hoe de winst en verliesrekening werkt, maak helder hoe de balans en cash flow dynamiek is van deze opportunity:
-	Wanneer is het noodzakelijk om resources te kopen, zoals hardware, software, mensen te contracteren, materialen?
-	Wanneer moet het bedrijf beginnen met mensen te betalen?
-	Wanneer gaat het bedrijf langdurige verplichtingen aan, zoals lease-auto’s, onderhoudscontracten, is dit onvermijdelijk?
-	Hoe lang duurt het voordat de eerste klant er is? 
 - Hoe lang duurt het voordat je de eerste rekening kunt sturen?
-	hoeveel kapitaal is nodig om een Euro aan verkopen support te leveren?

Leg uit hoe dicht er de buurt gekomen wordt van het ideaalplaatje; laag kopen, hoog verkopen, vroeg geld innen en laat uitgeven?

Maak je projecties realistisch voor een periode van vijf jaar. Wordt niet overdreven optimistisch. Maak het model niet nodeloos ingewikkeld en geef de investeerder toegang tot de dynamiek van de cijfers, door de variabelen te kunnen wijzigingen in de onderliggende spreadsheet.

## 3.4 Groeimogelijkheden

Demonstreer en analyseer hoe een kans kan groeien; uitbreiden van products & services, klanten base of geografische scope.

Welke afhankelijkheden (wat/wanneer) en hoe worden risico’s gemanaged om de bestaande business te behouden of te laten groeien?

## 3.5 Concurrentie

**Grondig doen.** 
Vanuit opportunisme hebben ondernemers de neiging dit te verwaarlozen. Alle kansen hebben een belofte en een kwetsbaarheid. Show you know the good, the bad and the ugly.

-	Wie zijn de concurrenten van deze onderneming?
-	Welke resources hebben zij? 
  - Wat zijn hun krachten en zwakheden?
-	Hoe zullen zij reageren op de onderneming die “hun business” afpakt en hoe kan de ondernemer reageren op hun reactie?
- Wie zou mogelijk nog meer deze opportunity (willen) pakken? 
  - Is het zinvol om allianties aan te gaan met concurrenten?

<br>

# 4. Context

Wat is mede van belang waar je geen invloed op hebt (macro-economische factoren, bijvoorbeeld schaarste aan personeel.)?

Welke regelgeving, subsidies en belastingen zijn van belang die mogelijk zouden kunnen wijzigingen?

Toon aan dat je voldoet aan eisen van deze tijd op het vlak van duurzaamheid, databeveiliging en diversiteit. Hoe wil je je onderscheiden op dit vlak?

Toon aan dat je je bewust bent van de context waarbinnen de opportunity bestaat en vooral dat je je bewust bent dat deze zal veranderen en hoe dat je business zal raken. Wat doe je als de context zich negatief ontwikkelt? 
Indien van toepassing; in hoeverre kun/wil je er invloed op uitoefenen (lobby o.i.d.)?

<br>

# 5. Risico en opbrengsten

Scenario’s

Geef een beeld van mensen/context/opportunity in verschillende toekomstscenario’s (het gaat altijd anders dan bedacht). Ontvouw mogelijkheden van actie en reactie.

Wat als:
-	Kijk naar verschillende projecten en de impact als deze veel meer of minder succesvol zijn dan gepland.
-	Nieuwe mensen niet functioneren, onvoldoende verkopen.
-	De ondernemer onder de tram komt of een key medewerker weg is.
-	Een concurrent aggressief toeslaat in belangrijke segmenten.
-	De rente enorm stijgt.
-	etc.

Timing
-	waarom is het nu een goed moment om in te stappen als investeerder?
-	waarom is het een goede timing om nu te internationaliseren met het product of de dienst?
-  Is het niet te laat of te vroeg?

Risico en opbrengst dynamiek

Het is verhelderend om risico en opbrengst te visualiseren middels twee plaatjes:
1. geld (verticaal) nodig om nieuwe venture te starten, tijd (horizontaal) tot break even, en de verwachtte omvang van de opbrengst. De oppervlakte geeft de hoeveelheid geld weer.
2. kans/roi, de kans (verticaal) op return per jaar (horizontaal, van –100 % tot meer dan 200 %). Realisme hier zorgt voor geloofwaardigheid.

Exit mogelijkheden

Wat zijn de exit scenario’s (als je weet waar je uitwilt komen, wordt de kans groter dat je daar uitkomt)?
-	Ideaal: brede range van exit opties, van strategische koper, een volgend fonds van venture capitalist tot een beursgang
-	Welke activiteiten gepland om opties open te houden of te genereren in komende jaren? 
-	Voor welke strategische koper is de onderneming interessant?
  - Welke prijs zou die er voor over hebben?
  - Vanaf welke schaal en wat zijn de criteria?
-	Hoe ziet het proces er uit voor ondernemer?
  - Fasering exits.
  - Welke rol wil de ondernemer vervullen na exit? wat drijft hem dan?
  - Wil hij iets anders gaan doen of doorbouwen? Hoe doet hij dat zodat ondernemer en koper enthousiast blijven?

<br>

# 6. Bijlagen en data

# 6.1 Bijlagen

Een soft copy van het budget wordt verstrekt in tweede fase, nadat er serieuze interesse is getoond. Dan wordt ook een map met overige bijlagen verstrekt.

-	Planning, “structure for fullfillment” per strategische doelstelling, met subdoelstellingen en eventuele onderliggen plannen, korte beschrijving van proces voor planning en monitoring.
-	Up to date salesforecast 
-	Indien beschikbaar beschrijving processen, practices, protocollen, en andere documenten, waarin werkwijzes zijn vastgelegd
- Team
  - CV’s kern team
  - Opleidingshistorie personeel en planning
- Risico's financieel
  - Copy jaarrekeningen laatste drie jaar
  - Toelichting overzicht niet uit balans blijkende verplichtingen
  - Eventuele overige rapporten accountant / derden
  - Verzekeringen 
  - Overzicht contracten, looptijd, opzegtermijn en toelichting hoe dit in prognose is verwerkt
  - Samenvatting betaalgedrag, termijnen, incasso, dubieuze debiteuren, toelichting
  - Issues met klanten of leverancier, waaruit mogelijk claims van derden volgen
  - Personele issues en mogelijke claims
  - Copy aandeelhoudersregister en aandeelhoudersovereenkomsten
  - Afspraken en status betalingen pensioenen en andere regelingen voor personeel
  - Andere financiële of juridische risico’s
- Technologie
  - Stabiliteit en back up faciliteiten van technologie (beschrijving, overzicht calamiteiten afgelopen jaar, kort commentaar maatregelen voor toekomst, klanteisen en risico’s en geplande activiteiten en investeringen)
  - Beschrijving en korte onderbouwing gemaakte technologie keuzes, welke keuzes in komende jaar te maken, welke risico’s en eventuele alternatieven
-	Schriftelijke klant referenties
-	Leaflets / brochures / productbeschrijvingen / offerteteksten / voorwaarden



## 6.2 Data

Voor inzage beschikbaar voor due diligence:
-	Bankafschriften
-	Documentatie betaalproces (facturering, herinnering)
-	Contracten en correspondentie klanten
-	Contracten en correspondentie leveranciers
-	Personele administratie, correspondentie, declaraties, salarisontwikkeling, arbeidsovereenkomsten, beoordelingen en afspraken, pensioen, secundaire beloningen, bonusafspraken, optieregelingen, ter beschikkingstelling apparatuur, onkostenvergoedingen
-	Correspondentie eerdere aandeelhouders, notulen van aandeelhoudersvergaderingen, historische voortgangsrapportages

## 6.3 Producten, diensten en applicaties

Uitleg en gebruik van eventuele user interfaces, testen van product.

## 6.4	Interviews en referenties

In overleg:
-	Eerdere werkgevers en zakelijke relaties management team
-	Klanten
-	Belangrijke leveranciers
-	Enkele key medewerkers

<br>

# 7.	Overige tips uit Harvard Business Reviews

## Price / dillution

Sahlman: “When I talk to young (and old) entrepreneurs looking to finance their ventures, they obsess about the valuation and terms of the deal they will receive. Their explicit goal seems to be to minimimize the dilution they will suffer in raising capital. Implicitely, they are also looking for investors who will remain passive. On the food chain of investors, it seems, doctors and dentists are best and venture capitalists are worst because of the degree the latter group demands control and a large share of the returns. That notion is nonsense. From whom you raise capital is often more important than the terms. New ventures are inherently risky, as I’ve note; what can go wrong will. When that happens, unsophisticated investors panic, get angry, and often refuse to advance the company more money. Sophisticated investors, by contrast, roll up their sleeves and help the company solve its problems. Often they’ve had lots of experiene saving sinking ships. They are typically process literate. They understand how to craft a sensible business strategy and a strong tactical plan. They know how to recruit, compensate, and motivate team members.... This kind of know how is worth the money needed to buy it.”

Rich&Gumpert: “To figure out how much to invest in your offering, investors calculate your company’s value on the asis of results expected five years after they invest. They’ll want a 35 tot 40 % return for mature companies – up to 60 % for less mature ventures.”


## Deal making

Verstandige deals zijn :
simpel, eerlijk, gebaseerd eerder op vertrouwen dan op juridische posities, vallen niet uit elkaar als de werkelijkheid enigzins afwijkt van het plan, hebben geen grote incentives in de toekomst waardoor contraproductief gedrag ontstaat, zijn niet meer dan een halve centimeter dik.

Niet statisch, eerder kijken naar hoeveel geld hebben we nodig en wanneer door de venture te beschouwen als een serie experimenten (bijv. experiment marktpositie pakken, experiment internationalisatie naar de UK, etc.). Voldoende financiering voor de eerste serie experimenten

Voorzie in het uitgeven van een portie van de taart voor (nieuw) key personeel, bij groei zijn er meer trekkers van de kar nodig.

## Openheid

Investeerders begrijpen dat er geen rendement is zonder risico. Wees open over waar je twijfel zit, wat de grootse risico’s zijn en hoe je dat denkt te managen.

----